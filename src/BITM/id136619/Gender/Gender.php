<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 11/1/2016
 * Time: 6:33 PM
 */

namespace App\Gender;
use App\Database as DB;
use App\Message\Message;
use App\Utility\Utility;

use PDO;

class Gender extends DB
{
    public $id;

    public $gender;



    public function __construct()
    {

        parent::__construct();

    }

    public function setData($data){

        if (array_key_exists('gender', $data)) {
            $this->gender = $data['gender'];
        }

    }


    public function store()
    {
        $DBH = $this->conn;
        $data = array('gender' => $this->gender);
        $STH = $DBH->prepare("insert into `gender` (`gender`) VALUES (:gender)");

        $STH->execute($data);

        Message::message("<div id='msg'></div><h3 align='center'>[ gender: $this->gender ] <br> Data Has Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');

    }
}