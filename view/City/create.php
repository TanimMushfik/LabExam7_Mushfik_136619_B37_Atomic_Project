<?php
require_once("../../vendor/autoload.php");
use App\Message\Message;

if(!isset( $_SESSION)) session_start();
echo Message::message();

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>City</title>
    <link rel="stylesheet" href="../../resource/css/style.css">

    <link rel="stylesheet" href="../../resource/bootstrap/css/bootstrap.min.css">

</head>
<body>
<h2>City</h2>
<form class="form-horizontal" action="store.php" method="post">
    <div class="form-group">
        <label class="control-label col-sm-2" for="pwd">City</label>
        <div class="col-sm-4">
            <select name="city">
                <option value="">Select</option>
                <option value="dhaka">Dhaka</option>
                <option value="chittagong">Chittagong</option>
                <option value="khulna">Khulna</option>
                <option value="barisal">Barisal</option>
                <option value="sylet">Shylet</option>
            </select>        </div>
    </div>
        <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                       <button type="submit" class="btn btn-info">Submit</button>
                   </div>
    </div>
<!--    <div class="form-group">-->
<!---->
<!--        <label class="control-label col-sm-2" >City</label>-->
<!--        <div class="col-sm-4">-->
<!--            <input type="text" name="city" class="form-control"  placeholder="City" size="10px">-->
<!--        </div>-->
<!--    </div>-->
<!---->
<!--    <div class="form-group">-->
<!--        <div class="col-sm-offset-2 col-sm-10">-->
<!---->
<!--        </div>-->
<!--    </div>-->
<!--    <div class="form-group">-->
<!--        <div class="col-sm-offset-2 col-sm-10">-->
<!--            <button type="submit" class="btn btn-info">Submit</button>-->
<!--        </div>-->
<!--    </div>-->
</form>
</body>
</html>




