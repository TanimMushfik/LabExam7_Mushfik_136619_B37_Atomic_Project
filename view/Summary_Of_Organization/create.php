<?php
require_once("../../vendor/autoload.php");
use App\Message\Message;

if(!isset( $_SESSION)) session_start();
echo Message::message();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Summary of Organization</title>
    <link rel="stylesheet" href="../../resource/css/style.css">

    <link rel="stylesheet" href="../../resource/bootstrap/css/bootstrap.min.css">
</head>
<body>
<h2>Create Book Title</h2>
<form class="form-horizontal" action="store.php" method="post">
    <div class="form-group">

        <label class="control-label col-sm-2" >Summary</label>
        <div class="col-sm-4">
            <textarea name="summary" id="" cols="60" rows="20"  placeholder="Summary"></textarea>
        </div>
    </div>
    <div class="form-group">

        <label class="control-label col-sm-2" >Organization</label>
        <div class="col-sm-4">
            <input type="text" name="organization" class="form-control"  placeholder="organization" >
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">

        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-info">Submit</button>
        </div>
    </div>
</form>
</body>
</html>




