<?php


require_once("../../vendor/autoload.php");
use App\Message\Message;

if(!isset( $_SESSION)) session_start();
echo Message::message();

?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add Book Title</title>
    <link rel="stylesheet" href="../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../resource/css/uikit.min.css">
    <link rel="stylesheet" href="../../resource/uikit.gradient.min.css">
    <link rel="stylesheet" href="../../resource/uikit.almost-flat.min.css">
    <link rel="stylesheet" href="../../resource/css/datepicker.almost-flat.min.css">
    <link rel="stylesheet" href="../../resource/css/style.css">

    <script src="../../resource/jquery.js"></script>
    <script src="../../resource/uikit.min.js"></script>
    <script src="../../resource/datepicker.min.js"></script>
</head>
<body>
<h2>Birthday Date</h2>

<form class="form-horizontal" method="post" action="store.php">
    <div class="form-group">
        <label class="control-label col-sm-2" for="name">Name:</label>
        <div class="col-sm-4">
            <input type="text" name="name" class="form-control" id="name" placeholder="Enter Name" size="10px">
        </div>
    </div>


  <div class="form-group">
        <label class="control-label col-sm-2" for="date">Enter date:</label>
        <div class="col-sm-4">
            <input type="text" name="birthday_date" data-uk-datepicker="{format:'DD.MM.YYYY'}">
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" name="submit" class="btn btn-info">Create</button>
        </div>
    </div>
</form>
</body>
</html>




