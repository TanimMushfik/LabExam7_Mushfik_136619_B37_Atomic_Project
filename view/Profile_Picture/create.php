<?php
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add Book Title</title>
    <link rel="stylesheet" href="../../resource/css/style.css">

    <link rel="stylesheet" href="../../resource/bootstrap/css/bootstrap.min.css">
</head>
<body>
<h2>Create Book Title</h2>
<form class="form-horizontal" method="post" action="store.php">

    <div class="form-group">
        <label class="control-label col-sm-2" for="pwd">Select image:</label>
        <div class="col-sm-4">
            <input type="file" name="file" class="form-control" id="file"  >
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" name="submit" class="btn btn-info">Upload</button>
        </div>
    </div>
</form>
</body>
</html>




