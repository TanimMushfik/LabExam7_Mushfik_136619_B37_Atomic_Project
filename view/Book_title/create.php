<?php



require_once("../../vendor/autoload.php");
use App\Message\Message;

if(!isset( $_SESSION)) session_start();
echo Message::message();



?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add Book Title</title>
    <link rel="stylesheet" href="../../resource/css/style.css">

    <link rel="stylesheet" href="../../resource/bootstrap/css/bootstrap.min.css">
</head>
<body>
<h2>Create Book Title</h2>
<form class="form-horizontal" action="store.php" method="post">
    <div class="form-group">

        <label class="control-label col-sm-2" >Book Title</label>
        <div class="col-sm-4">
            <input type="text" name="book_title" class="form-control"  placeholder="Book Title" size="10px">
        </div>
    </div>
    <div class="form-group">

        <label class="control-label col-sm-2" >Author Name</label>
        <div class="col-sm-4">
            <input type="text" name="author_name" class="form-control"  placeholder="Author Name" >
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">

        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-info">Submit</button>
        </div>
    </div>
</form>
</body>
</html>




